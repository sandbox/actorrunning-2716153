
-- INTRODUCTION --

This modules adds eCheck integration to the Commerce Authorize.Net module.


-- REQUIREMENTS --

Commerce Authorize.Net


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure the Authorize.Net AIM - eCheck.Net rule as for any other commerce
  payment method.
